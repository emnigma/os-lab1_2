//
// Created by Professional on 20.10.2020.
//

#ifndef OS_LAB1_2_ASYNC_COPY_H
#define OS_LAB1_2_ASYNC_COPY_H

#pragma once
#include <windows.h>
#include <iostream>
#include <string>

using namespace std;

void asyncCopyOfFile();
VOID WINAPI asyncRead(DWORD Code, DWORD nBytes, LPOVERLAPPED lpOv);
VOID WINAPI asyncWrite(DWORD Code, DWORD nBytes, LPOVERLAPPED lpOv);

#endif //OS_LAB1_2_ASYNC_COPY_H
