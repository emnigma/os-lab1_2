//
// Created by Professional on 20.10.2020.
//

#ifndef OS_LAB1_2_ASYNC_COPY_CPP
#define OS_LAB1_2_ASYNC_COPY_CPP

#include "async_copy.h"

using namespace std;

// каждый thread отвечает за свою часть файла, поэтому в коллбэке asyncWrite снова вызывается asyncRead,
// при этом проверяется условие не перехода за границу файла

int numbersOfByteToWrite;
int bufferMultiplier;
CHAR **buffersArray;
OVERLAPPED *overlapIn, *overlapOut;
HANDLE original, copyFile;
LARGE_INTEGER fileSize, endOfFile;
LONGLONG  doneCount, recordCount;

void asyncCopyOfFile() {
    int overlapOperationsCount = 0; // количество перекрывающихся операций
    numbersOfByteToWrite = 4096; // размер наименьшего блока
    bufferMultiplier = 1; // для удобства тестирования

    std::string sourceFile, destinationFile;

    cout << "path to file: ";
    cin >> sourceFile;

    original = CreateFile(sourceFile.c_str(), GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_FLAG_OVERLAPPED | FILE_FLAG_NO_BUFFERING, NULL);
    if (original == INVALID_HANDLE_VALUE) {
        throw std::invalid_argument("cant open original file");
    }

    cout << "path to copy: ";
    cin >> destinationFile;

    copyFile = CreateFile(destinationFile.c_str(), GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_FLAG_OVERLAPPED | FILE_FLAG_NO_BUFFERING, NULL);
    if (original == INVALID_HANDLE_VALUE) {
        throw std::invalid_argument("cant open file to copy");
    }

    cout << "overlapping operations count: ";
    cin >> overlapOperationsCount;
    cout << "buff mult: ";
    cin >> bufferMultiplier;
    numbersOfByteToWrite *= bufferMultiplier;
    cout << "block equals = " << numbersOfByteToWrite << " bytes" << endl;

    // массив буферов для временнй записи блока памяти
    buffersArray = new CHAR*[overlapOperationsCount];
    for (int i = 0; i<overlapOperationsCount; i++)
        buffersArray[i] = new CHAR[numbersOfByteToWrite];

    // инициализация управляющих структур для копирования и записи объектов
    overlapIn = new OVERLAPPED[overlapOperationsCount];
    overlapOut = new OVERLAPPED[overlapOperationsCount];

    GetFileSizeEx(original, &fileSize);

    cout << "File size = " << fileSize.QuadPart << " bytes" << endl;

    recordCount = fileSize.QuadPart / numbersOfByteToWrite;
    if ((fileSize.QuadPart % numbersOfByteToWrite) != 0)
        ++recordCount;
    cout << "Blocks count = " << recordCount << endl;

    DWORD startCopyTime, endCopyTime;
    startCopyTime = GetTickCount();
    // указатель на смещение для работы с текущим потоком
    LARGE_INTEGER curPosIn;
    curPosIn.QuadPart = 0;

    for (int i = 0; i < overlapOperationsCount; ++i) {

        // нужны для идентификации OVERLAPPED-структуры внутри callback-функции
        overlapIn[i].hEvent = (HANDLE)i;
        overlapOut[i].hEvent = (HANDLE)i;

        overlapIn[i].Offset = curPosIn.LowPart;
        overlapIn[i].OffsetHigh = curPosIn.HighPart;
        if (curPosIn.QuadPart < fileSize.QuadPart)
            ReadFileEx(original, buffersArray[i], numbersOfByteToWrite, &overlapIn[i], asyncRead);
        curPosIn.QuadPart += (LONGLONG)numbersOfByteToWrite;
    }

    doneCount = 0;
    while (doneCount < 2 * recordCount)
        // пока не завершится операция копирования-записи:
        SleepEx(INFINITE, true);

    cout << "Copy successful!" << endl;

    delete[] overlapIn;
    delete[] overlapOut;
    for (int i = 0; i < overlapOperationsCount; ++i)
        delete[]buffersArray[i];
    delete[] buffersArray;

    endOfFile.QuadPart = fileSize.QuadPart;
//    endOfFile.HighPart = fileSize.HighPart;

    SetFilePointerEx(copyFile, endOfFile, NULL, FILE_BEGIN);
    SetEndOfFile(copyFile);

    CloseHandle(original);
    CloseHandle(copyFile);
    endCopyTime = GetTickCount();
    cout << "Time spent: " << endCopyTime - startCopyTime << " ms" << endl;
}

VOID WINAPI asyncWrite(DWORD Code, DWORD nBytes, LPOVERLAPPED lpOv){

    ++doneCount;

    LARGE_INTEGER curPosIn;
    DWORD i = (DWORD)(lpOv->hEvent);

//    curPosIn.LowPart = overlapIn[i].Offset;
//    curPosIn.HighPart = overlapIn[i].OffsetHigh;
    curPosIn.QuadPart = overlapIn[i].OffsetHigh;

    if (curPosIn.QuadPart < fileSize.QuadPart){
        ReadFileEx(original, buffersArray[i], numbersOfByteToWrite, &overlapIn[i], asyncRead);
    }
}

VOID WINAPI asyncRead(DWORD Code, DWORD nBytes, LPOVERLAPPED lpOv) {
    //перемещение указателя на блок данных далее для этого потока

    ++doneCount;

    LARGE_INTEGER curPosIn, curPosOut;
    DWORD i = (DWORD)(lpOv->hEvent);

    curPosIn.LowPart = overlapIn[i].Offset;
    curPosIn.HighPart = overlapIn[i].OffsetHigh;

    curPosOut.QuadPart = curPosIn.QuadPart;

    overlapOut[i].Offset = curPosOut.LowPart;
    overlapOut[i].OffsetHigh = curPosOut.HighPart;

    WriteFileEx(copyFile, buffersArray[i], numbersOfByteToWrite, &overlapOut[i], asyncWrite);

    curPosIn.QuadPart += (LONGLONG)numbersOfByteToWrite;

    overlapIn[i].Offset = curPosIn.LowPart;
    overlapIn[i].OffsetHigh = curPosIn.HighPart;
}

#endif //OS_LAB1_2_ASYNC_COPY_CPP
